import React    from 'react';
import Screen   from './screen.jsx';

import pad      from '../utils';
export default class Display extends React.Component {

  constructor(props){
    super(props);
    this.state = { counter: 0 };
  }

  componentDidMount(){
    this.interval = setInterval(
      this.increment.bind(this),
      1000
    )
  }

  resetCounter() {
    this.setState( ({counter}) =>
      ({
        counter: 0
      })
    );
  }

  increment(){
    this.setState( ({counter}) =>
      ({
        counter: counter + 1, 
      })
    );
  }

  render() {

    return  <div className="display" onClick={this.resetCounter.bind(this)}> 
              <div className='indicator' />
              <Screen />
              <div className='counter-block' >
                <div> { this.state.counter } </div>
              </div>
            </div>;
  }
}







/*





<div className='indicator'    style={divStyle} />


let   indicatorWidthVW  = this.state.counter;
const screenWidthVW     = 60;
let divStyle= {
  width: indicatorWidthVW + 'vw',
  transform: 'translate(' +  ((screenWidthVW - indicatorWidthVW)/2) + 'vw , 0)',
}


counter: counter > 60 ? 0 : counter + 1


<div> { pad(this.state.counter, 6) } </div>


*/




