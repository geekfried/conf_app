import React                  from 'react';

export default class RedBlueLights extends React.Component {
  render() {
    return  <div className="red-blue-lights"> 
              <div className='blue-light'> <i className='material-icons'> my_location </i> </div>
              <div className='red-light' > <i className='material-icons'> view_list </i> </div>
              
            </div>;
  }
}

