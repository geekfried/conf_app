import React         from 'react';

import RedBlueLights from './red-blue-light.jsx';

import Display       from './display.jsx';

export default class App extends React.Component {
  render() {
    return  <div className="main"> 
              <RedBlueLights />
              <Display />
            </div>;
  }
}



