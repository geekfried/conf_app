import React                                    from 'react';
import ReactDOM                                 from 'react-dom';
import $                                        from 'jquery';
import _                                        from 'lodash';

import App    from './components/app';

import './css/app.scss';

ReactDOM.render(
  <App />
  ,
  document.getElementById('app'));
